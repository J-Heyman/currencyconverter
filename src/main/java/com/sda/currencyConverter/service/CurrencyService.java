package com.sda.currencyConverter.service;

import com.sda.currencyConverter.model.CurrencySymbol;
import com.sda.currencyConverter.model.Rate;
import com.sda.currencyConverter.model.TableType;
import com.sda.currencyConverter.repository.CurrencyRepo;
import org.apache.commons.math3.util.Precision;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class CurrencyService {
    public final static DateTimeFormatter DTF = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    private static Rate getCurrencyRate(CurrencySymbol currencySymbol, String effectiveDate) throws IOException {
        while (true) {
            try {
                return CurrencyRepo.getCurrencyRates(currencySymbol, effectiveDate).getRates()[0];
            } catch (FileNotFoundException e) {
                effectiveDate = DTF.format(LocalDate.parse(effectiveDate, DTF).minusDays(1L));
            }
        }
    }

    public Double getSpecificRate
            (CurrencySymbol currencySymbol, TableType tableType, String effectiveDate) throws IOException {

        switch (tableType) {
            case MID:
                return getCurrencyRate(currencySymbol, effectiveDate).getMid();
            case ASK:
                return getCurrencyRate(currencySymbol, effectiveDate).getAsk();
            case BID:
                return getCurrencyRate(currencySymbol, effectiveDate).getBid();
            default:
                return 1.0;
        }
    }

    public String getCurrencyDate(CurrencySymbol currencySymbol, String effectiveDate) throws IOException {
        int attempts = 1;
        while (true) {
            try {
                return CurrencyRepo.getCurrencyRates(currencySymbol, effectiveDate).getRates()[0].getEffectiveDate();
            } catch (FileNotFoundException e) {
                if (attempts++ > 7) {
                    throw new IOException();
                }
                effectiveDate = DTF.format(LocalDate.parse(effectiveDate, DTF).minusDays(1L));
            }
        }
    }

    public boolean checkTableTypes(String tableType) {
        for (TableType type : TableType.values()) {
            if (tableType.equalsIgnoreCase(type.toString())) {
                return true;
            }
        }
        return false;
    }

    public boolean checkCurrencySymbol(String currencySymbol) {
        for (CurrencySymbol currency : CurrencySymbol.values()) {
            if (currencySymbol.equalsIgnoreCase(currency.toString())) {
                return true;
            }
        }
        return false;
    }

    public Double convertPlnToCurrency(Double plnAmount, Double buyRate) {
        return Precision.round(plnAmount / buyRate, 2);
    }

    public Double convertCurrencyToPln(Double currencyAmount, Double sellRate) {
        return Precision.round(currencyAmount * sellRate, 2);
    }

    public Double getExchangeBalanceInPln(Double buyPrice, Double sellPrice) {
        return Precision.round(sellPrice - buyPrice, 2);
    }
}
