package com.sda.currencyConverter.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class Rate {
    private String no;
    private String effectiveDate;
    private Double mid;
    private Double ask;
    private Double bid;
}
