package com.sda.currencyConverter.model;

// Aktualnie obowiązujący kurs średni waluty CHF w formacie JSON
// http://api.nbp.pl/api/exchangerates/rates/a/chf/?format=json
// Notowanie kursu kupna i sprzedaży USD z 2016-04-04 w formacie JSON
// http://api.nbp.pl/api/exchangerates/rates/c/usd/2016-04-04/?format=json

public enum TableType {
    MID, ASK, BID
}
