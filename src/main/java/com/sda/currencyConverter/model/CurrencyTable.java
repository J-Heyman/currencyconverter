package com.sda.currencyConverter.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class CurrencyTable {
    private String table;
    private String currency;
    private String code;
    private Rate[] rates;
}
