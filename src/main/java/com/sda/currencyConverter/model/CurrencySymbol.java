package com.sda.currencyConverter.model;

public enum CurrencySymbol {
    USD, EUR, GBP, CHF
}
