package com.sda.currencyConverter.cli;

import com.sda.currencyConverter.model.CurrencySymbol;
import com.sda.currencyConverter.model.TableType;
import com.sda.currencyConverter.service.CurrencyService;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Scanner;

import static com.sda.currencyConverter.model.TableType.*;
import static com.sda.currencyConverter.service.CurrencyService.DTF;

public class CurrencyCli {
    private CurrencyService service = new CurrencyService();

    public void runApp() {
        Scanner read = new Scanner(System.in);
        String selection = "_";
        printMenu();

        while (selection.charAt(0) != 'e') {
            System.out.print("> ");
            selection = read.nextLine();

            if (selection.equals("")) {
                selection = "_";
            }

            try {
                switch (selection.toLowerCase().charAt(0)) {
                    case 'a':
                        System.out.println("▼");
                        printAllCurrenciesRates(MID, null);
                        break;
                    case 'b':
                        System.out.println("▼");
                        printAllCurrenciesRates(BID, null);
                        break;
                    case 'c':
                        System.out.println("▼");
                        printSelectedRatesOfTheSelectedCurrency();
                        break;
                    case 'd':
                        System.out.println("▼");
                        printCalculatedBalanceOfAnySelectedCurrencyOverSelectedDates();
                        break;
                    case 'm':
                        printMenu();
                        break;
                    case 'e':
                        break;
                    default:
                        System.out.println("Wrong selection! Try again!");
                        break;
                }
            } catch (IOException e) {
                System.out.println("SERVER ERROR!");
                e.getMessage();
            }
            System.out.println("════════════════════════");
        }
        System.out.println("END OF THE APP");
    }

    private void printMenu() {
        System.out.println("MENU:");
        System.out.println("a) Print MID rates of the all currencies.");
        System.out.println("b) Print BID rates of the all currencies.");
        System.out.println("c) Print selected type of rates of the selected currency.");
        System.out.println("d) Print calculated balance of any amount of the selected currency over selected dates.");
        System.out.println("m) Print Menu");
        System.out.println("e) End of the App");
        System.out.println("─────────────────────────────────────────────────────────────────────────────────────────");
        System.out.println("(write appropriate letter from the MENU options)");
    }

    private void printAllCurrenciesRates(TableType tableType, String effectiveDate) throws IOException {
        for (CurrencySymbol currencySymbol : CurrencySymbol.values()) {
            printCurrencyRate(currencySymbol, tableType, effectiveDate);
        }
    }

    private void printCurrencyRate
            (CurrencySymbol currencySymbol, TableType tableType, String effectiveDate) throws IOException {
        Double rate = service.getSpecificRate(currencySymbol, tableType, effectiveDate);
        String date = service.getCurrencyDate(currencySymbol, effectiveDate);

        System.out.printf("► %s > %s %s/PLN = %.4f %n", date, tableType, currencySymbol, rate);
    }


    private void printCurrencyBalanceOfPlnAmountOverSelectedDates
            (Double plnAmount, CurrencySymbol currencySymbol, String buyDate, String sellDate) throws IOException {

        Double buyRate = service.getSpecificRate(currencySymbol, ASK, buyDate);
        Double sellRate = service.getSpecificRate(currencySymbol, BID, sellDate);
        Double buyResult = service.convertPlnToCurrency(plnAmount, buyRate);
        Double sellResult = service.convertCurrencyToPln(buyRate, sellRate);

        Double operationResult = service.getExchangeBalanceInPln(buyResult, sellResult);

        buyDate = service.getCurrencyDate(currencySymbol, buyDate);
        sellDate = service.getCurrencyDate(currencySymbol, sellDate);

        System.out.printf("► %.2f PLN = %.2f %s (%s, %s)%n", plnAmount, buyResult, currencySymbol, ASK, buyDate);
        System.out.printf("► %.2f %s = %.2f PLN (%s, %s)%n", buyResult, currencySymbol, sellResult, BID, sellDate);
        System.out.printf("► %s = %.2f%n", (operationResult < 0 ? "Loss" : "Profit"), operationResult);
    }

    private void printSelectedRatesOfTheSelectedCurrency() throws IOException {
        printCurrencyRate(getCurrencySymbolFromUser(), getTableTypeFromUser(), null);
    }

    private void printCalculatedBalanceOfAnySelectedCurrencyOverSelectedDates() throws IOException {
        boolean validDates = true;

        while (validDates) {
            LocalDate buyDate = getDateFromUser("buy");
            LocalDate sellDate = getDateFromUser("sell");

            if (buyDate.isBefore(sellDate)) {
                validDates = false;
                printCurrencyBalanceOfPlnAmountOverSelectedDates
                        (getAmountFromUser(), getCurrencySymbolFromUser(), DTF.format(buyDate), DTF.format(sellDate));
            } else {
                System.err.println("Wrong date! Date can't be in future. Try again!");
                validDates = true;
            }
        }
    }

    private LocalDate getDateFromUser(String dateDescription) {
        Scanner read = new Scanner(System.in);
        LocalDate userDate;

        while (true) {
            System.out.print("Write " + dateDescription + " date in format (yyyy-mm-dd): ");
            try {
                userDate = LocalDate.parse(read.nextLine(), DTF);
                if (userDate.isBefore(LocalDate.now())) {
                    return userDate;
                } else {
                    System.out.println("Wrong date! Date can't be after today. Try again!");
                }
            } catch (Exception e) {
                System.out.println("Wrong date! Try again!");
            }
        }
    }

    private Double getAmountFromUser() {
        Scanner read = new Scanner(System.in);

        while (true) {
            System.out.print("Write amount of money to exchange: ");
            try {
                return Double.parseDouble(read.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("Wrong number! Try again");
            }
        }
    }

    private CurrencySymbol getCurrencySymbolFromUser() {
        Scanner read = new Scanner(System.in);
        String currencySymbol = "";
        boolean check = true;

        while (check) {
            System.out.print("Write type of currency [" + getListOfCurrencySymbols() + "]: ");

            currencySymbol = read.nextLine();
            check = !service.checkCurrencySymbol(currencySymbol);
            if (check) {
                System.out.println("Wrong currency symbol! Try again!");
            }
        }

        return CurrencySymbol.valueOf(currencySymbol.toUpperCase());
    }

    private TableType getTableTypeFromUser() {
        Scanner read = new Scanner(System.in);
        String tableType = "";
        boolean check = true;

        while (check) {
            System.out.print("Write type of rate [" + getListOfTableTypes() + "]: ");

            tableType = read.nextLine();
            check = !service.checkTableTypes(tableType);
            if (check) {
                System.out.println("Wrong type of rate! Try again!");
            }
        }

        return TableType.valueOf(tableType.toUpperCase());
    }

    private String getListOfCurrencySymbols() {
        return StringUtils.joinWith(", ", CurrencySymbol.values());
    }

    private String getListOfTableTypes() {
        return StringUtils.joinWith(", ", TableType.values());
    }

}
