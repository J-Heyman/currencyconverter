package com.sda.currencyConverter;

import com.sda.currencyConverter.cli.CurrencyCli;

public class AppCurrencyConverter {
    public static void main(String[] args) {
        new CurrencyCli().runApp();
    }
}
