package com.sda.currencyConverter.repository;

import com.google.gson.Gson;
import com.sda.currencyConverter.model.CurrencySymbol;
import com.sda.currencyConverter.model.CurrencyTable;

import java.io.IOException;
import java.net.URL;
import java.util.Scanner;

public class CurrencyRepo {
    private final static String NBP_URL = "http://api.nbp.pl/api/exchangerates/rates/";

    public static CurrencyTable getCurrencyRates
            (CurrencySymbol currencySymbol, String effectiveDate) throws IOException {

        String bidAskUrl = getSpecificUrl(currencySymbol, effectiveDate, "c");
        String midUrl = getSpecificUrl(currencySymbol, effectiveDate, "a");

        String askBidJsonText = getDataFromServer(bidAskUrl);
        String midJsonText = getDataFromServer(midUrl);

        CurrencyTable rates = getObjectFromJson(askBidJsonText);
        CurrencyTable midRates = getObjectFromJson(midJsonText);

        rates.getRates()[0].setMid(midRates.getRates()[0].getMid());

        return rates;
    }

    private static String getSpecificUrl(CurrencySymbol currencySymbol, String effectiveDate, String nbpTable) {
        return new StringBuilder().append(NBP_URL)
                .append(nbpTable).append("/")
                .append(currencySymbol).append("/")
                .append(effectiveDate == null ? "" : effectiveDate)
                .append(effectiveDate == null ? "" : "/")
                .append("?format=json")
                .toString();
    }

    private static String getDataFromServer(String link) throws IOException {
        URL url = new URL(link);
        Scanner response = new Scanner(url.openStream());
        StringBuilder responseText = new StringBuilder();

        while (response.hasNext()) {
            responseText.append(response.nextLine());
        }

        return responseText.toString();
    }

    private static CurrencyTable getObjectFromJson(String jsonText) {
        return new Gson().fromJson(jsonText, CurrencyTable.class);
    }

}
