package com.sda.tests.currencyConverter.service;


import com.sda.currencyConverter.service.CurrencyService;
import org.junit.Test;

import java.io.IOException;

import static com.sda.currencyConverter.model.CurrencySymbol.USD;
import static com.sda.currencyConverter.model.TableType.MID;
import static org.junit.Assert.assertEquals;

public class CurrencyServiceTest {
    private CurrencyService service = new CurrencyService();

    @Test
    public void getSpecificRate_test() throws IOException {
        assertEquals((Double) 3.8543, service.getSpecificRate(USD, MID, null));
    }

    @Test
    public void getCurrencyDate_test_1() throws IOException {
        assertEquals("2019-05-13", service.getCurrencyDate(USD, "2019-05-13"));
    }

    @Test
    public void getCurrencyDate_test_2() throws IOException {
        // Because 2019-05-12 is a Sunday than actual date is from 2019-05-10 (Friday)
        assertEquals("2019-05-10", service.getCurrencyDate(USD, "2019-05-12"));
    }

    @Test
    public void checkTableTypes_test() {
        assertEquals(true, service.checkTableTypes("mid"));
    }

    @Test
    public void checkCurrencySymbol_test_1() {
        assertEquals(true, service.checkTableTypes("usd"));
    }

    @Test
    public void checkCurrencySymbol_test_2() {
        assertEquals(false, service.checkTableTypes("abc"));
    }

    @Test
    public void convertPlnToCurrency_test() {
        assertEquals((Double)25.95, service.convertPlnToCurrency(100.0, 3.8543));
    }

    @Test
    public void convertCurrencyToPln_test() {
        assertEquals((Double)385.43, service.convertCurrencyToPln(100.0, 3.8543));
    }

    @Test
    public void getExchangeBalanceInPln_Test() {
        assertEquals((Double)(-1.46), service.getExchangeBalanceInPln(100.0, 98.54));
    }

    // Add more tests if need...
}